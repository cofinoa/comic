[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/geoocean%2Fcourses%2Fcomic/master)

# Computing in Civil Engineering

Fernando J. Méndez Incera (fernando.mendez@unican.es)\
Alba Ricondo Cueva (alba.ricondo@unican.es)

<a name="ins"></a>
## Install
- - -
<a name="ins_src"></a>
### Install from sources

Install requirements. Navigate to the base root of [comic](./) and execute:

```
# Default install, miss some dependencies and functionality
pip install -r requirements/requirements.txt
```
